#ANGULAR
    https://angular.io/
#LOOPBACK
    https://loopback.io/
#IONIC
    https://ionicframework.com/getting-started
#DOCKER
    https://hub.docker.com

    #Imágenes Docker
        pull: docker pull image:tag
        list: docker images
        remove: docker rmi nombre_o_id_imagen 
        ver imágenes sin tags: docker images -f "dangling=true"
        sólo ids: docker images -q
        borrar todas: docker rmi $(docker images -q)


    #Crear contenedor docker para la base de datos mysql
        docker hub: https://hub.docker.com/_/mysql/
        pull: docker pull mysql:5.7
        run: docker run -d -p 3306:3306 --name db-mysql -e MYSQL_ROOT_PASSWORD=secret mysql:5.7
        exec: docker exec -it db-mysql mysql -uroot -p
        stop: docker stop db-mysql
        start: docker start db-mysql
        restart: docker restart db-mysql
        remove: docker rm mysql
        stats: docker stats
        logs: docker logs -f db-mysql
        volume:  docker volume ls
        docker volume create app01db
        docker run -d -p 3306:3306 --name db-mysql -e MYSQL_ROOT_PASSWORD=secret -v app01db:/var/lib/mysql mysql:5.7

#NPM
    https://www.npmjs.com/

