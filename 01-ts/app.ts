import { Persona, Empleado } from "./entidades"

let persona = new Persona('Ana', 'Perez');
persona.mayorEdad = true;
console.log('Instancia de persona:', persona)
let empleado: Empleado = new Empleado('test', 'app')
empleado.antiguedad = 3;
console.log(empleado)