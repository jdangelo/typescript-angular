export class Persona {
    private _mayorEdad: boolean = false;
    get mayorEdad(): boolean {
        return this._mayorEdad;
    }
    set mayorEdad(mayor: boolean) {
        this._mayorEdad = mayor;
    }
    nombre: string;
    apellido: string;
    email?: string;
    constructor(nombre: string, apellido: string, email?: string, edad: number = 22) {
        this.nombre = nombre;
        this.apellido = apellido;
        this.email = email;
    }
}